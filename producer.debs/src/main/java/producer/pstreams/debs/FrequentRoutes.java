package producer.pstreams.debs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.log4j.BasicConfigurator;

import producer.pstreams.debs.utils.IncompletePropertiesException;
import producer.pstreams.debs.utils.ProducerConfUtils;
import producer.pstreams.debs.utils.TripFields;
import producer.pstreams.debs.utils.TripUtils;

public class FrequentRoutes {

	public static final String INPUT_FILE = "";

	public static boolean finished = false;

	public static void main(String[] args) {

		if (args.length != 4) {
			System.out.println("Usage: java <program> " + "<kafka_server_list (ip1:port1, ip2:port2)> "
					+ "<zookeeper_address (ip:port)> " + "<input_file> <time_rate>");
			System.exit(-1);
		}

		String kafkaServers = args[0];
		String zookeeperAddress = args[1];
		String input = args[2];
		long timeRate = Integer.parseInt(args[3]);

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				finished = true;
			}
		});

		BasicConfigurator.configure();

		ProducerConfUtils.setZookeeperConnect(zookeeperAddress);
		ProducerConfUtils.setKafkaServers(kafkaServers);

		Properties props = null;
		try {
			props = ProducerConfUtils.getProps();
		} catch (IncompletePropertiesException e1) {
			e1.printStackTrace();
			assert (false);
		}

		KafkaProducer<String, String> producer = new KafkaProducer<String, String>(props);

		// Shows the statistics at the same rate as the time rate (PS: check if this doesn't screw up the whole benchmark)
		Stats stats = new Stats(timeRate);

		try {

			while (!finished) {
				File file = new File(input);
				BufferedReader reader = new BufferedReader(new FileReader(file));
				String trip;

				String currentDate = null;

				while ((trip = reader.readLine()) != null && !finished) {
					String dropOffTime = TripFields.dropOffTime.getField(TripFields.separate(trip));

					if (currentDate == null) {
						currentDate = dropOffTime;
					}

					if (!dropOffTime.equals(currentDate)) {
						Date d1 = null;
						Date d2 = null;
						try {
							d1 = TripUtils.getDate(currentDate);
							d2 = TripUtils.getDate(dropOffTime);
						} catch (ParseException e) {
							e.printStackTrace();
						}

						// it is possible to have erroneous values since the
						// time to process this might not be negligible.

						// time difference in milliseconds
						long diff = d2.getTime() - d1.getTime();
						long sleepTime = diff / timeRate;

						try {
							Thread.sleep(sleepTime);
						} catch (InterruptedException e) {
							assert (false);
						}

						currentDate = dropOffTime;
					}

					// sends the message to the kafka cluster
					ProducerRecord<String, String> record = new ProducerRecord<String, String>(
							ProducerConfUtils.TRIP_TOPIC, trip);
					// KeyedMessage<String, String> data = new
					// KeyedMessage<>(ProducerConfUtils.TRIP_TOPIC, "value",
					// trip);
					long sendStart = System.currentTimeMillis();
					Callback cb = stats.nextCompletion(sendStart, trip.getBytes().length, stats);
					producer.send(record, cb);
				}

				reader.close();
			}

			producer.close();
			stats.printTotal();

		} catch (FileNotFoundException e) {
			System.out.println("File was not found");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Could not read file");
			e.printStackTrace();
		}
	}

	private static class Stats {
		private long start;
		private long windowStart;
		private int iteration;
		private long count;
		private long bytes;
		private int maxLatency;
		private long totalLatency;
		private long windowCount;
		private int windowMaxLatency;
		private long windowTotalLatency;
		private long windowBytes;
		private double reportingInterval;

		public Stats(double reportingInterval) {
			this.start = System.currentTimeMillis();
			this.windowStart = System.currentTimeMillis();
			this.iteration = 0;
			this.maxLatency = 0;
			this.totalLatency = 0;
			this.windowCount = 0;
			this.windowMaxLatency = 0;
			this.windowTotalLatency = 0;
			this.windowBytes = 0;
			this.totalLatency = 0;
			this.reportingInterval = reportingInterval;
		}

		public void record(int iter, int latency, int bytes, long time) {
			this.count++;
			this.bytes += bytes;
			this.totalLatency += latency;
			this.maxLatency = Math.max(this.maxLatency, latency);
			this.windowCount++;
			this.windowBytes += bytes;
			this.windowTotalLatency += latency;
			this.windowMaxLatency = Math.max(windowMaxLatency, latency);

			// Maybe report the recent performance
			if (time - windowStart >= reportingInterval) {
				printWindow();
				newWindow();
			}
		}

		public Callback nextCompletion(long start, int bytes, Stats stats) {
			Callback cb = new PerfCallback(this.iteration, start, bytes, stats);
			this.iteration++;
			return cb;
		}

		public void printWindow() {
			long ellapsed = System.currentTimeMillis() - windowStart;
			double recsPerSec = 1000.0 * windowCount / (double) ellapsed;
			double mbPerSec = 1000.0 * this.windowBytes / (double) ellapsed / (1024.0 * 1024.0);
			System.out.printf(
					"%d records sent, %.1f records/sec (%.2f MB/sec), %.1f ms avg latency, %.1f max latency.\n",
					windowCount, recsPerSec, mbPerSec, windowTotalLatency / (double) windowCount,
					(double) windowMaxLatency);
		}

		public void newWindow() {
			this.windowStart = System.currentTimeMillis();
			this.windowCount = 0;
			this.windowMaxLatency = 0;
			this.windowTotalLatency = 0;
			this.windowBytes = 0;
		}

		public void printTotal() {
			long ellapsed = System.currentTimeMillis() - start;
			double recsPerSec = 1000.0 * count / (double) ellapsed;
			double mbPerSec = 1000.0 * this.bytes / (double) ellapsed / (1024.0 * 1024.0);
			System.out.printf(
					"Total: %d records sent, %f records/sec (%.2f MB/sec), %.2f ms avg latency, %.2f ms max latency.\n",
					count, recsPerSec, mbPerSec, totalLatency / (double) count, (double) maxLatency);
		}
	}

	private static final class PerfCallback implements Callback {
		private final long start;
		private final int iteration;
		private final int bytes;
		private final Stats stats;

		public PerfCallback(int iter, long start, int bytes, Stats stats) {
			this.start = start;
			this.stats = stats;
			this.iteration = iter;
			this.bytes = bytes;
		}

		public void onCompletion(RecordMetadata metadata, Exception exception) {
			long now = System.currentTimeMillis();
			int latency = (int) (now - start);
			this.stats.record(iteration, latency, bytes, now);
			if (exception != null)
				exception.printStackTrace();
		}
	}
}
