package producer.pstreams.debs.utils;

public class GridUtils {

	// for 500.500 m cells
	private static final double firstCellCenterLat = 41.474937;
	private static final double firstCellCenterLon = -74.913585;

	// for 500.500 m cells
	private static final double latCellSize = 0.005986;
	private static final double lonCellSize = 0.004481556;

	private static final double firstCellLat = firstCellCenterLat - latCellSize / 2.0;
	private static final double firstCellLon = firstCellCenterLon - lonCellSize / 2.0;

	private static final double latCellSizeKm = latCellSize * 2;
	private static final double lonCellSizeKm = lonCellSize * 2;

	private static final double getStepLat(double meters) {
		return latCellSizeKm / (meters / 1000.0);
	}

	private static final double getStepLon(double meters) {
		return lonCellSizeKm / (meters / 1000.0);
	}
	
	private static double defaultLatCellSize = latCellSizeKm;
	private static double defaultLonCellSize = lonCellSizeKm;
	
	/**
	 * Sets the default cell size in meters
	 *   We assume that all cells are squares.
	 * @param meters - the size of a cell side.
	 */
	public static void setDefaultCellSize (double meters) {
		defaultLatCellSize = getStepLat(meters);
		defaultLonCellSize = getStepLon(meters);
	}
	
	/**
	 * Returns the cell latitude index
	 * @param lat - a latitude value
	 * @return the latitude index of the cell
	 */
	public static int getCellLat(double lat) {
		return 1 + (int) Math.floor((lat - firstCellLat) / defaultLatCellSize);
	}

	/**
	 * Returns the cell longitude index
	 * @param lon - a longitude value
	 * @return the longitude index of the cell
	 */
	public static int getCellLon(double lon) {
		return 1 + (int) Math.floor((lon - firstCellLon) / defaultLonCellSize);
	}
}
