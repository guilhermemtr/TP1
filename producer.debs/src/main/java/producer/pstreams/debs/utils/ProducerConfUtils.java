package producer.pstreams.debs.utils;

import java.util.Properties;

import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

public class ProducerConfUtils {

	public static final String TRIP_TOPIC = "trips";
	
	// Check if error using Properties instead of HashMap<String,String>
	private static final Properties props = new Properties();
	private static boolean zookeeperConnectSet = false;
	private static boolean kafkaServersSet = false;

	static {
		props.put("serializer.class", "kafka.serializer.StringEncoder");
		props.put("key.serializer", org.apache.kafka.common.serialization.StringSerializer.class);
		props.put("value.serializer", org.apache.kafka.common.serialization.StringSerializer.class);
		props.put("request.required.acks", "1");
	}

	public static final void setZookeeperConnect(String zookeeper) {
		zookeeperConnectSet = true;
		props.put("zookeeper.connect", zookeeper);
	}
	
	public static final void setKafkaServers(String kafkaServers) {
		kafkaServersSet = true;
		props.put("bootstrap.servers", kafkaServers);
	}

	public static final Properties getProps() throws IncompletePropertiesException {
		if (zookeeperConnectSet && kafkaServersSet) {
			return props;
		} else {
			throw new IncompletePropertiesException();
		}
	}

	private static final String getMasterSparkConf(int numThreads) {
		return "local[" + numThreads + "]";
	}

	private static SparkConf sconf = null;

	public static final void setConf(int numThreads, String applicationName) {
		sconf = new SparkConf().setMaster(getMasterSparkConf(numThreads)).setAppName(applicationName);
	}

	public static final SparkConf getSparkConf() {
		return sconf;
	}

	public static final JavaStreamingContext getStreamingContext(SparkConf sparkConf, long secs) {
		return new JavaStreamingContext(sparkConf, Durations.seconds(secs));
	}
	
	public static final Duration getStreamingDuration(){
		return Durations.minutes(30);
	}

}