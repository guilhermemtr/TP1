package consumer.pstreams.debs.frequent_routes.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

public class ConsumerConfUtils {

	public static final String TRIP_TOPIC = "trips";

	private static final Set<String> topics = new HashSet<String>();

	static {
		topics.add(TRIP_TOPIC);
	}

	public static final Set<String> getTopics() {
		return topics;
	}

	private static final Map<String, String> props = new HashMap<String, String>();
	private static boolean zookeeperConnectSet = false;
	private static boolean groupIdSet = false;
	private static boolean brokerListSet = false;

	static {
		props.put("zookeeper.session.timeout.ms", "40000");
		props.put("zookeeper.sync.time.ms", "200");
		props.put("auto.commit.interval.ms", "1000");
	}

	public static final void setZookeeperConnect(String zookeeper) {
		zookeeperConnectSet = true;
		props.put("zookeeper.connect", zookeeper);
	}

	public static final void setGroupId(String groupId) {
		groupIdSet = true;
		props.put("group.id", groupId);
	}

	public static final void setBrokerList(String brokerList) {
		brokerListSet = true;
		props.put("metadata.broker.list", brokerList);
	}

	public static final Map<String, String> getProps() throws IncompletePropertiesException {
		if (zookeeperConnectSet && groupIdSet && brokerListSet) {
			return props;
		} else {
			throw new IncompletePropertiesException();
		}
	}

	private static SparkConf sconf = null;

	public static final void setConf(String master, String applicationName) {
		sconf = new SparkConf().setMaster(master).setAppName(applicationName);
	}

	public static final SparkConf getSparkConf() {
		return sconf;
	}

	public static final JavaStreamingContext getStreamingContext(SparkConf sparkConf, long secs) {
		return new JavaStreamingContext(sparkConf, Durations.seconds(secs));
	}
}