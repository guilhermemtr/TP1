package consumer.pstreams.debs.profitable_areas.utils;

public enum TripFields {
	medallion, hackLicense, pickupTime, dropOffTime, tripTime, tripDistance,
	pickupLon, pickupLat, dropoffLon, dropoffLat, paymentType, fareAmount,
	surCharge, taxes, tipAmount, tollsAmount, totalAmount;
	
	public static String[] separate(String trip) {
		return trip.split(",");
	}
	
	public String getField(String[] trip, int offset) {
		return trip[offset + this.ordinal()];
	}
	
	public String getField(String[] trip) {
		return getField(trip,0);
	}
}
