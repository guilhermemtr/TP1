import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import kafka.serializer.StringDecoder;
import scala.Tuple2;
import utils.*;

public class RoutesConsumer1 {

	public static final long sparkDuration = 30L;
	public static final long cellSize = 500L;

	public RoutesConsumer1() {
	}

	//runs the query
	public void run(JavaStreamingContext sctx, Map<String, String> props, Set<String> topics) {
		JavaPairInputDStream<String, String> kafkaStream = KafkaUtils.createDirectStream(sctx, String.class,
				String.class, StringDecoder.class, StringDecoder.class, props, topics);

		kafkaStream.window(Durations.minutes(30), Durations.seconds(30))
				.map(s -> mapToCells(s._2()))
				.filter(RoutesConsumer1::checkOutliers)
				.mapToPair(s -> new Tuple2<>(s, 1))
				.reduceByKey((a, b) -> a + b)
				.transformToPair(rdd -> rdd.mapToPair(s -> new Tuple2<>(s._2(), s._1()))
						.sortByKey(false).mapToPair(s -> new Tuple2<>(s._2(), s._1())))
				.foreachRDD(RoutesConsumer1::showTop10);
	}

	private static boolean checkOutliers(String trip) {
		String[] fields = CellFields.separate(trip);
		for(CellFields c: CellFields.values()) {
			int cell = Integer.parseInt(c.getField(fields));
			if(GridUtils.isOutlier(cell))
				return false;
		}
		return true;
	}

	private static String mapToCells(String trip) {
		String[] fields = TripFields.separate(trip);
		int pickupLon = GridUtils.getCellLon(Double.parseDouble(TripFields.pickupLon.getField(fields)));
		int pickupLat = GridUtils.getCellLat(Double.parseDouble(TripFields.pickupLat.getField(fields)));
		int dropoffLon = GridUtils.getCellLon(Double.parseDouble(TripFields.dropoffLon.getField(fields)));
		int dropoffLat = GridUtils.getCellLat(Double.parseDouble(TripFields.dropoffLat.getField(fields)));
		return pickupLon + "," + pickupLat + "," + dropoffLon + "," + dropoffLat;
	}

	private static Void showTop10(JavaPairRDD<String, Integer> result) {
		Iterator<Tuple2<String,Integer>> iterator = result.toLocalIterator();

		System.out.println("New updates:");
		for (int n = 0; n < 10; n++) {
			if(iterator.hasNext()) {
				Tuple2<String, Integer> entry = iterator.next();
				System.out.println(entry._1() + " " + entry._2());
			} else {
				System.out.println("Null Null");
			}
		}
		return null;
	}

	//sets up and calls the run method
	public static void main(String[] args) {

		// checks the input arguments
		if (args.length != 4) {
			System.out.println("Usage: java <program> <broker_list (ip1:port1, ip2:port2)> "
					+ "<zookeeper_address (ip:port)> <group_id> <master>");
			System.exit(-1);
		}

		// reads the arguments
		String brokerList = args[0];// "172.17.42.1:32869";
		String zookeeper = args[1]; // "172.17.42.1:32865";
		String groupId = args[2]; // "badjoras";
		String master  = args[3];

		// Creates and sets the spark configuration
		ConsumerConfUtils.setConf(master, "FrequentRoutes");

		GridUtils.setDefaultCellSize(cellSize);

		// creates a java streaming context
		JavaStreamingContext sctx = ConsumerConfUtils.getStreamingContext(ConsumerConfUtils.getSparkConf(),
				sparkDuration);

		// what is this?
		sctx.checkpoint("./checkpoint");

		// sets up the consumer configuration settings
		ConsumerConfUtils.setZookeeperConnect(zookeeper);
		ConsumerConfUtils.setGroupId(groupId);
		ConsumerConfUtils.setBrokerList(brokerList);

		// gets the consumer topics to which it should subscribe
		Set<String> topics = ConsumerConfUtils.getTopics();

		Map<String, String> props = null;
		try {
			props = ConsumerConfUtils.getProps();
		} catch (IncompletePropertiesException e) {
			System.err.println("Some Properties have not been set.");
			System.exit(-1);
		}

		RoutesConsumer1 rc = new RoutesConsumer1();

		rc.run(sctx, props, topics);

		long startTime = System.currentTimeMillis();
		sctx.start();
		sctx.awaitTermination();
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		System.out.println("Duration:\t" + duration);
	}
}