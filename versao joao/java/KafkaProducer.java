import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import org.apache.log4j.BasicConfigurator;

import utils.*;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

public class KafkaProducer {

	public static boolean finished = false;

	public static void main(String[] args) {

		if (args.length != 4) {
			System.out.println("Usage: java <program> " + "<broker_list (ip1:port1, ip2:port2)> "
					+ "<zookeeper_address (ip:port)> " + "<input_file> <time_rate>");
			System.exit(-1);
		}

		String brokerList = args[0];
		String zookeeperAddress = args[1];
		String input = args[2];
		long timeRate = Integer.parseInt(args[3]);

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				finished = true;
			}
		});

		BasicConfigurator.configure();

		ProducerConfUtils.setBrokerList(brokerList);
		ProducerConfUtils.setZookeeperConnect(zookeeperAddress);

		ProducerConfig config = null;

		try {
			config = new ProducerConfig(ProducerConfUtils.getProps());
		} catch (IncompletePropertiesException e1) {
			e1.printStackTrace();
			assert (false);
		}

		Producer<String, String> producer = new Producer<>(config);

		try {

			while (!finished) {
				File file = new File(input);
				BufferedReader reader = new BufferedReader(new FileReader(file));
				String trip;

				String currentDate = null;

				while ((trip = reader.readLine()) != null && !finished) {
					String dropOffTime = TripFields.dropOffTime.getField(TripFields.separate(trip));

					if (currentDate == null) {
						currentDate = dropOffTime;
					}

					if (!dropOffTime.equals(currentDate)) {
						Date d1 = null;
						Date d2 = null;
						try {
							d1 = TripUtils.getDate(currentDate);
							d2 = TripUtils.getDate(dropOffTime);
						} catch (ParseException e) {
							e.printStackTrace();
						}

						// it is possible to have erroneous values since the
						// time to process this might not be negligible.

						// time difference in milliseconds
						long diff = d2.getTime() - d1.getTime();
						long sleepTime = diff / timeRate;

						try {
							Thread.sleep(sleepTime);
						} catch (InterruptedException e) {
							assert (false);
						}

						currentDate = dropOffTime;
					}

					// sends the message to the kafka cluster
					KeyedMessage<String, String> data = new KeyedMessage<>(ProducerConfUtils.TRIP_TOPIC, "value", trip);
					producer.send(data);
				}

				reader.close();
			}

			producer.close();

		} catch (FileNotFoundException e) {
			System.out.println("File was not found");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Could not read file");
			e.printStackTrace();
		}
	}
}
