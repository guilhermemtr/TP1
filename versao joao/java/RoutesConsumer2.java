import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import com.google.common.base.Optional;

import utils.*;
import kafka.serializer.StringDecoder;
import scala.Tuple2;

public class RoutesConsumer2 {

	public static final long sparkDuration = 30L;
	public static final long cellSize = 250L;

	private RoutesConsumer2() {
	}

	// runs the query
	public void run(JavaStreamingContext sctx, Map<String, String> props, Set<String> topics) {

		JavaPairInputDStream<String, String> kafkaStream = KafkaUtils
				.createDirectStream(sctx, String.class, String.class, StringDecoder.class, StringDecoder.class, props, topics);

		JavaPairDStream<String, String> filteredTrips = kafkaStream
				.filter(RoutesConsumer2::isOutlier);

		JavaPairDStream<String, Double> profitByTrip = filteredTrips.mapToPair(s -> {
			String trip = s._2();
			String[] fields = TripFields.separate(trip);
			String cell = mapToCells(fields);
			Double fare = Double.parseDouble(TripFields.fareAmount.getField(fields));
			Double tip = Double.parseDouble(TripFields.tipAmount.getField(fields));
			return new Tuple2<String, Double>(cell, fare + tip);
		}).window(Durations.minutes(15), Durations.seconds(30));

		JavaPairDStream<String, Long> count = profitByTrip
				.map(RoutesConsumer2::cell)
				.countByValue();

		JavaPairDStream<String, Double> sumProfit = profitByTrip
				.reduceByKey(RoutesConsumer2::reduceProfit);

		JavaPairDStream<String, Double> cellAverageProfit = count
				.join(sumProfit)
				.mapToPair(RoutesConsumer2::getAverageProfit);

		JavaPairDStream<String, Long> emptyTaxis = filteredTrips
				.mapToPair(RoutesConsumer2::getMedallionCellMapping)
				.updateStateByKey(updateFunction)
				.window(Durations.minutes(30), Durations.seconds(30))
				.mapToPair(RoutesConsumer2::mapToCellCount)
				.reduceByKey(RoutesConsumer2::reduceCellCount);

		JavaPairDStream<String, Double> result = cellAverageProfit
				.join(emptyTaxis)
				.mapToPair(RoutesConsumer2::getAreaProfit)
				.transformToPair(rdd -> rdd.mapToPair(s -> new Tuple2<>(s._2(), s._1()))
						.sortByKey(false).mapToPair(s -> new Tuple2<>(s._2(), s._1())));

		result.foreachRDD(RoutesConsumer2::showTop10);
		// filteredTrips
		// .mapToPair(s -> new Tuple2<>(s, 1)).reduceByKey((a, b) -> a + b)
		// .transformToPair(rdd -> rdd.sortByKey(false)).
		// filtered.foreachRDD(this::showTop10);

	}

	private static Function2<List<String>, Optional<String>, Optional<String>> updateFunction = new Function2<List<String>, Optional<String>, Optional<String>>() {
		@Override
		public Optional<String> call(List<String> values, Optional<String> state) {
			String newDropoff = "";
			if(state.isPresent()) newDropoff = state.get();
			for (String v : values) {
				newDropoff = v;
			}
			return Optional.of(newDropoff);
		}
	};

	private static Tuple2<String, Double> getAreaProfit(Tuple2<String, Tuple2<Double, Long>> t3) {
		String cell = t3._1();
		Tuple2<Double, Long> t = t3._2();
		return new Tuple2<String, Double>(cell, (double) t._1 / (double) (t._2));
	}

	private static Tuple2<String, Double> getAverageProfit(Tuple2<String, Tuple2<Long, Double>> t3) {
		String cell = t3._1();
		Tuple2<Long, Double> t = t3._2();
		return new Tuple2<String, Double>(cell, (double) t._2 / (double) (t._1));
	}

	private static Long reduceCellCount(Long v1, Long v2) {
		return v1 + v2;
	}

	private static Tuple2<String, Long> mapToCellCount(Tuple2<String, String> t) {
		return new Tuple2<String, Long>(t._2(), 1L);
	}

	private static Tuple2<String, String> getMedallionCellMapping(Tuple2<String, String> t) {
		String trip = t._2();
		String[] fields = TripFields.separate(trip);
		return new Tuple2<String, String>(TripFields.medallion.getField(fields), mapToCells(fields));
	}

	private static Double reduceProfit(Double d1, Double d2) {
		return d1 + d2;
	}

	private static String cell(Tuple2<String, Double> tuple) {
		return tuple._1();
	}

	private static boolean isOutlier(Tuple2<String, String> t) {
		String[] fields = TripFields.separate(t._2());
		if (GridUtils.isOutlier(GridUtils.getCellLon(Double.parseDouble(TripFields.pickupLon.getField(fields))))) {
			return false;
		}

		if (GridUtils.isOutlier(GridUtils.getCellLat(Double.parseDouble(TripFields.pickupLat.getField(fields))))) {
			return false;
		}

		if (GridUtils.isOutlier(GridUtils.getCellLon(Double.parseDouble(TripFields.dropoffLon.getField(fields))))) {
			return false;
		}

		if (GridUtils.isOutlier(GridUtils.getCellLat(Double.parseDouble(TripFields.dropoffLat.getField(fields))))) {
			return false;
		}
		return true;
	}

	// private static boolean checkOutliers(String trip) {
	// String cells = mapToCells(trip);
	// String[] fields = CellFields.separate(cells);
	// for (CellFields c : CellFields.values()) {
	// int cell = Integer.parseInt(c.getField(fields,
	// TripFields.values().length));
	// if (GridUtils.isOutlier(cell))
	// return false;
	// }
	// return true;
	// }

	private static String mapToCells(String[] fields) {
		int pickupLon = GridUtils.getCellLon(Double.parseDouble(TripFields.pickupLon.getField(fields)));
		int pickupLat = GridUtils.getCellLat(Double.parseDouble(TripFields.pickupLat.getField(fields)));
		int dropoffLon = GridUtils.getCellLon(Double.parseDouble(TripFields.dropoffLon.getField(fields)));
		int dropoffLat = GridUtils.getCellLat(Double.parseDouble(TripFields.dropoffLat.getField(fields)));
		return pickupLon + "," + pickupLat + "," + dropoffLon + "," + dropoffLat;
	}

	private static Void showTop10(JavaPairRDD<String, Double> result) {
		Iterator<Tuple2<String,Double>> iterator = result.toLocalIterator();

		System.out.println("New updates:");
		for (int n = 0; n < 10; n++) {
			if(iterator.hasNext()) {
				Tuple2<String, Double> entry = iterator.next();
				System.out.println(entry._1() + " " + entry._2());
			} else {
				System.out.println("Null Null");
			}
		}
		return null;
	}

	// sets up and calls the run method
	public static void main(String[] args) {

		// checks the input arguments
		if (args.length != 4) {
			System.out.println("Usage: java <program> <broker_list (ip1:port1, ip2:port2)> "
					+ "<zookeeper_address (ip:port)> <group_id> <master>");
			System.exit(-1);
		}

		// reads the arguments
		String brokerList = args[0];// "172.17.42.1:32869";
		String zookeeper = args[1]; // "172.17.42.1:32865";
		String groupId = args[2]; // "badjoras";
		String master = args[3];

		// Creates and sets the spark configuration
		ConsumerConfUtils.setConf(master, "KafkaProducer");

		GridUtils.setDefaultCellSize(cellSize);

		// creates a java streaming context
		JavaStreamingContext sctx = ConsumerConfUtils.getStreamingContext(ConsumerConfUtils.getSparkConf(), sparkDuration);

		// what is this?
		sctx.checkpoint("./checkpoint");

		// sets up the consumer configuration settings
		ConsumerConfUtils.setZookeeperConnect(zookeeper);
		ConsumerConfUtils.setGroupId(groupId);
		ConsumerConfUtils.setBrokerList(brokerList);

		// gets the consumer topics to which it should subscribe
		Set<String> topics = ConsumerConfUtils.getTopics();

		Map<String, String> props = null;
		try {
			props = ConsumerConfUtils.getProps();
		} catch (IncompletePropertiesException e) {
			System.err.println("Some Properties have not been set.");
			System.exit(-1);
		}

		RoutesConsumer2 rc = new RoutesConsumer2();

		rc.run(sctx, props, topics);

		long startTime = System.currentTimeMillis();
		sctx.start();
		sctx.awaitTermination();
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		System.out.println("Duration:\t" + duration);
	}
}