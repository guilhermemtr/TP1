package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TripUtils {
	
	private static final SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd HH:mm:ss"); //2013-01-01 00:00:00

	public static Date getDate(String date) throws ParseException {
		return format.parse(date);
	}
	
}
