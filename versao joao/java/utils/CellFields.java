package utils;

public enum CellFields {
	pickupLon, pickupLat, dropoffLon, dropoffLat;

	public static String[] separate(String trip) {
		return trip.split(",");
	}

	public String getField(String[] trip, int offset) {
		return trip[offset + this.ordinal()];
	}

	public String getField(String[] trip) {
		return getField(trip, 0);
	}
}
