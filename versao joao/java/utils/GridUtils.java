package utils;

public class GridUtils {

	// for 500.500 m cells
	private static final double firstCellCenterLat = 41.474937;
	private static final double firstCellCenterLon = -74.913585;

	// for 500.500 m cells
	private static final double latCellSize = 0.005986;
	private static final double lonCellSize = 0.004491556;

	
	private static final double cellSizeMeters = 500.0;

	private static long gridcells500M = 300L;
	
	private static final double firstCellLat = firstCellCenterLat - latCellSize / 2.0;
	private static final double firstCellLon = firstCellCenterLon - lonCellSize / 2.0;

	private static final double latCellSizeKm = latCellSize * 2;
	private static final double lonCellSizeKm = lonCellSize * 2;
	
	private static long gridCells = gridcells500M;

	private static final double getStepLat(double meters) {
		return latCellSizeKm / (meters / 1000.0);
	}

	private static final double getStepLon(double meters) {
		return lonCellSizeKm / (meters / 1000.0);
	}
	
	private static double defaultLatCellSize = latCellSizeKm;
	private static double defaultLonCellSize = lonCellSizeKm;
	
	/**
	 * Sets the default cell size in meters
	 *   We assume that all cells are squares.
	 * @param meters - the size of a cell side.
	 */
	public static void setDefaultCellSize (double meters) {
		defaultLatCellSize = getStepLat(meters);
		defaultLonCellSize = getStepLon(meters);
		gridCells = (long) (meters * ((double)gridcells500M) / ((double)cellSizeMeters));

		System.out.println("GridUtils: New cell size "+defaultLatCellSize+" "+defaultLonCellSize+" "+gridCells);
	}
	
	/**
	 * Returns the cell latitude index
	 * @param lat - a latitude value
	 * @return the latitude index of the cell
	 */
	public static int getCellLat(double lat) {
		return 1 + (int) Math.floor((firstCellLat - lat) / defaultLatCellSize);
	}

	/**
	 * Returns the cell longitude index
	 * @param lon - a longitude value
	 * @return the longitude index of the cell
	 */
	public static int getCellLon(double lon) {
		return 1 + (int) Math.floor((lon - firstCellLon) / defaultLonCellSize);
	}
	
	/**
	 * Given the number of a cell, this method returns whether it is an outlier or not.
	 * @param id the id of the cell
	 * @return whether the cell outlies or not on the grid.
	 */
	public static boolean isOutlier(int id) {
		return !(id > 0 && id <= gridCells);
	}
}
